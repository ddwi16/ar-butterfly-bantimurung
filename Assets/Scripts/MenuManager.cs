using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
  private void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape))
    {
      BtnEscape();
    }
  }

  public void BtnEscape()
  {
    Application.Quit(0);
  }

  public void BtnAugmented()
  {
    SceneManager.LoadScene("Gameplay");
  }

  public void BtnHome()
  {
    SceneManager.LoadScene("Menu");
  }

  public void BtnOpenMaps()
  {
    Application.OpenURL("https://goo.gl/maps/EATzitDnRiG28mVc6");
  }

  public void BtnOpenGuide()
  {
    Application.OpenURL("https://drive.google.com/drive/folders/1VRxYwfipof2rdvfCam7kUPNc_V_Ywt52?usp=sharing");
  }
}
