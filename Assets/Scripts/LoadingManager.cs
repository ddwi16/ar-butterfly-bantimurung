using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{
  [SerializeField] float waitLoadToMenu;

  private void Awake()
  {
    StartCoroutine(WaitLoading(waitLoadToMenu));
  }

  IEnumerator WaitLoading(float num)
  {
    yield return new WaitForSeconds(num);
    SceneManager.LoadScene("Splash Screen");
  }
}
