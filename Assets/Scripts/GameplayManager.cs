using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
  [SerializeField]
  GameObject panelSpesies, panelPakan, panelMetamorfosis;
  [SerializeField]
  GameObject jenisFamilyUI, spesiesLangkaUI, pakanUI, metamorfosisUI;
  [SerializeField]
  GameObject danaidaeUI, lycanidaeUI,
  montUI, nhympalidaeUI,
  papilionidaeUI, pieridaeUI, satyridaeUI;
  [SerializeField]
  GameObject hypolitusUI, holiphronUI, helenaUI, chetosiaUI;


  //DELETE THIS AWAKE LATER
  private void Awake()
  {
    PlayerPrefs.SetInt("danaidae", 0);
    PlayerPrefs.SetInt("lycanidae", 0);
    PlayerPrefs.SetInt("mont", 0);
    PlayerPrefs.SetInt("nhympalidae", 0);
    PlayerPrefs.SetInt("papilionidae", 0);
    PlayerPrefs.SetInt("pieridae", 0);
    PlayerPrefs.SetInt("satyridae", 0);

    PlayerPrefs.SetInt("hypolitus", 0);
    PlayerPrefs.SetInt("holiphron", 0);
    PlayerPrefs.SetInt("helena", 0);
    PlayerPrefs.SetInt("chetosia", 0);
  }

  private void Update()
  {
    print("danaidae " + PlayerPrefs.GetInt("danaidae") + ", " +
    "lycanidae " + PlayerPrefs.GetInt("lycanidae") + ", " +
    "mont " + PlayerPrefs.GetInt("mont") + ", " +
    "nhympalidae " + PlayerPrefs.GetInt("nhympalidae") + ", " +
    "papilionidae " + PlayerPrefs.GetInt("papilionidae") + ", " +
    "pieridae " + PlayerPrefs.GetInt("pieridae") + ", " +
    "satyridae " + PlayerPrefs.GetInt("satyridae"));

    print("hypolitus " + PlayerPrefs.GetInt("hypolitus") + ", " +
    "holiphron " + PlayerPrefs.GetInt("holiphron") + ", " +
    "helena " + PlayerPrefs.GetInt("helena") + ", " +
    "chetosia " + PlayerPrefs.GetInt("chetosia") + ", ");
  }

  public void BtnJenisFamily()
  {
    jenisFamilyUI.SetActive(true);
    spesiesLangkaUI.SetActive(false);
    pakanUI.SetActive(false);
    metamorfosisUI.SetActive(false);
  }

  public void BtnSpesiesLangka()
  {
    if (PlayerPrefs.GetInt("danaidae") == 1 &&
    PlayerPrefs.GetInt("lycanidae") == 1 &&
    PlayerPrefs.GetInt("mont") == 1 &&
    PlayerPrefs.GetInt("nhympalidae") == 1 &&
    PlayerPrefs.GetInt("papilionidae") == 1 &&
    PlayerPrefs.GetInt("pieridae") == 1 &&
    PlayerPrefs.GetInt("satyridae") == 1)
    {
      panelSpesies.SetActive(false);
      spesiesLangkaUI.SetActive(true);
      jenisFamilyUI.SetActive(false);
      pakanUI.SetActive(false);
      metamorfosisUI.SetActive(false);
    }
    else
    {
      panelSpesies.SetActive(true);
    }
  }

  public void BtnPakan()
  {
    if (PlayerPrefs.GetInt("hypolitus") == 1 &&
    PlayerPrefs.GetInt("holiphron") == 1 &&
    PlayerPrefs.GetInt("helena") == 1 &&
    PlayerPrefs.GetInt("chetosia") == 1)
    {
      panelPakan.SetActive(false);
      pakanUI.SetActive(true);
      spesiesLangkaUI.SetActive(false);
      jenisFamilyUI.SetActive(false);
      metamorfosisUI.SetActive(false);
    }
    else
    {
      panelPakan.SetActive(true);
    }
  }

  public void BtnMetamorfosis()
  {
    panelMetamorfosis.SetActive(true);
  }

  #region FAMILY_BUTTERFLY_HANDLER
  public void BtnDanaidae()
  {
    PlayerPrefs.SetInt("danaidae", 1);
  }

  public void BtnLycanidae()
  {
    PlayerPrefs.SetInt("lycanidae", 1);
  }

  public void BtnMont()
  {
    PlayerPrefs.SetInt("mont", 1);
  }

  public void BtnNhympalidae()
  {
    PlayerPrefs.SetInt("nhympalidae", 1);
  }

  public void BtnPapilionidae()
  {
    PlayerPrefs.SetInt("papilionidae", 1);
  }

  public void BtnPeridae()
  {
    PlayerPrefs.SetInt("pieridae", 1);
  }

  public void BtnSatyridae()
  {
    PlayerPrefs.SetInt("satyridae", 1);
  }

  public void ShowSatyridae()
  {
    if (PlayerPrefs.GetInt("satyridae") == 1)
    {
      satyridaeUI.SetActive(false);
    }
    else
    {
      satyridaeUI.SetActive(true);
    }
  }

  public void ShowPeridae()
  {
    if (PlayerPrefs.GetInt("pieridae") == 1)
    {
      pieridaeUI.SetActive(false);
    }
    else
    {
      pieridaeUI.SetActive(true);
    }
  }

  public void ShowPapilionidae()
  {
    if (PlayerPrefs.GetInt("papilionidae") == 1)
    {
      papilionidaeUI.SetActive(false);
    }
    else
    {
      papilionidaeUI.SetActive(true);
    }
  }

  public void ShowNhympalidae()
  {
    if (PlayerPrefs.GetInt("nhympalidae") == 1)
    {
      nhympalidaeUI.SetActive(false);
    }
    else
    {
      nhympalidaeUI.SetActive(true);
    }
  }

  public void ShowMont()
  {
    if (PlayerPrefs.GetInt("mont") == 1)
    {
      montUI.SetActive(false);
    }
    else
    {
      montUI.SetActive(true);
    }
  }

  public void ShowLycanidae()
  {
    if (PlayerPrefs.GetInt("lycanidae") == 1)
    {
      lycanidaeUI.SetActive(false);
    }
    else
    {
      lycanidaeUI.SetActive(true);
    }
  }

  public void ShowDanaidae()
  {
    if (PlayerPrefs.GetInt("danaidae") == 1)
    {
      danaidaeUI.SetActive(false);
    }
    else
    {
      danaidaeUI.SetActive(true);
    }
  }
  #endregion

  #region SPESIES_LANGKA_HANDLER
  public void BtnHypolitus()
  {
    PlayerPrefs.SetInt("hypolitus", 1);
  }

  public void ShowHypolitus()
  {
    if (PlayerPrefs.GetInt("hypolitus") == 1)
    {
      hypolitusUI.SetActive(false);
    }
    else
    {
      hypolitusUI.SetActive(true);
    }
  }

  public void BtnHoliphron()
  {
    PlayerPrefs.SetInt("holiphron", 1);
  }

  public void ShowHoliphron()
  {
    if (PlayerPrefs.GetInt("holiphron") == 1)
    {
      holiphronUI.SetActive(false);
    }
    else
    {
      holiphronUI.SetActive(true);
    }
  }

  public void BtnHelena()
  {
    PlayerPrefs.SetInt("helena", 1);
  }

  public void ShowHelena()
  {
    if (PlayerPrefs.GetInt("helena") == 1)
    {
      helenaUI.SetActive(false);
    }
    else
    {
      helenaUI.SetActive(true);
    }
  }

  public void BtnChetosia()
  {
    PlayerPrefs.SetInt("chetosia", 1);
  }

  public void ShowChetosia()
  {
    if (PlayerPrefs.GetInt("chetosia") == 1)
    {
      chetosiaUI.SetActive(false);
    }
    else
    {
      chetosiaUI.SetActive(true);
    }
  }
  #endregion
}